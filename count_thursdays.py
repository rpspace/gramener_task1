import calendar


def count_thursdays(year1, year2):
    total_years = 0
    leap_years = 0
    if year2 >= year1:
        total_years = year2 - year1 + 1
        leap_years = calendar.leapdays(year1, year2)
    else:
        total_years = year1 - year2 + 1
        leap_years = calendar.leapdays(year2, year1)

    normal_years = total_years - leap_years
    no_of_days = normal_years * 365 + leap_years * 366

    return int(no_of_days / 7)


print("no of thursdays between 1990 & 2000=", count_thursdays(1990, 2000))
