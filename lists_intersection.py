def lists_intersection(L1, L2):
    if len(L1) >= len(L2):
        return list(filter(lambda x: x in L1, L2))
    else:
        return list(filter(lambda x: x in L2, L1))


L1 = ['a', 'b', 'c']
L2 = ['b', 'd']

print("Common elements L1 = ['a', 'b', 'c'] L2 = ['b', 'd'] is =", lists_intersection(L2, L1))
